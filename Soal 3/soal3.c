#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <signal.h>
#include <wait.h>
#include <stdbool.h>
#include <pwd.h>
#include <grp.h>

void delete_substr(char *str, char *substr){
    char *comp;
    int png = strlen(substr);
    while((comp = strstr(str, substr))){
        *comp = '\0';
        strcat(str, comp+png);
    }
}

void create_dir(){
    pid_t ch = fork();
    int status;

    if(ch == 0){
        char *argv[] = {"mkdir", "-p", "Oki/Sisop/Modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }else{
        while((wait(&status)) > 0);
        sleep(3);
        char *argv[] = {"mkdir", "-p", "Oki/Sisop/Modul2/air", NULL};
        execv("/bin/mkdir", argv);
    }
}

void unzip(){

    int status;

    if(fork() == 0){
        char *argv[] = {"unzip", "animal.zip", "-d", "Oki/Sisop/Modul2/", NULL};
        execv("/bin/unzip", argv);
    }
    while((wait(&status)) > 0 );
}

void move_animals(){
    DIR *dir;
    struct dirent *dp;
    
    char from[PATH_MAX];
    char dest[PATH_MAX];

    strcpy(from, "Oki/Sisop/Modul2/animal/");
    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            char fileName[NAME_MAX];
            strcpy(fileName, dp->d_name);
            delete_substr(fileName, ".jpg");

            strcpy(from, "Oki/Sisop/Modul2/animal/");
            strcat(from, dp->d_name);
            strcpy(dest, "Oki/Sisop/Modul2/");

            pid_t child = fork();
            int status;
            
            char *token;
            token = strtok(fileName, "_");
            int flag = 0;
            while(token != NULL){

                if(flag == 1){
                    if(strcmp(token, "air") == 0){
                        //printf("AIR: %s --> %s\n\n", from, dest);
                        strcat(dest, "air");
                        //printf("air %s --> %s\n", from, dest);

                        if(child == 0){
                            char *argv[] = {"mv", from, dest, NULL};
                            execv("/bin/mv", argv);
                        }
                        while((wait(&status)) > 0);
                    }

                    if(strcmp(token, "darat") == 0 || strcmp(token, "bird") == 0){
                        // printf("DARAT: %s --> %s\n\n", from, dest);
                        strcat(dest, "darat");
                        // printf("darat %s --> %s\n", from, dest);
                        if(child == 0){
                            char *argv[] = {"mv", from, dest, NULL};
                            execv("/bin/mv", argv);
                        }
                        while((wait(&status)) > 0);
                    }
                }

                token = strtok(NULL, "_");
                flag++;
            }

            if(child == 0){
                char *argv[] = {"rm", "-rf", from, NULL};
                execv("/bin/rm", argv);
            }
            while((wait(&status)) > 0);
        }
    }
}

void delete_bird(){
    DIR *dir;
    struct dirent *dp;
    
    char path[PATH_MAX];

    strcpy(path, "Oki/Sisop/Modul2/darat/");
    dir = opendir(path);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            strcpy(path, "Oki/Sisop/Modul2/darat/");
            strcat(path, dp->d_name);
            pid_t child = fork();
            int status;

            if(strstr(dp->d_name, "bird") != 0){
                if(child == 0){
                    char *argv[] = {"rm", "-rf", path, NULL};
                    execv("/bin/rm", argv);
                }
                while((wait(&status)) > 0);
            }
        }
    }
}

void create_list(){
    FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "Oki/Sisop/Modul2/air/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);

    // permission
    p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    // owner
    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
            strcat(path, dp->d_name);

            char owner[NAME_MAX];
            char p_r[NAME_MAX];
            char p_w[NAME_MAX];
            char p_x[NAME_MAX];
            
            // owner
            struct passwd *pw = getpwuid(info.st_uid);
            if (pw != 0) strcpy(owner, pw->pw_name); //puts(pw->pw_name);

            // permission
            if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
            if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
            if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

            if(strstr(dp->d_name, "list") == 0){
                fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
            }
        }
    }

    fclose(fp);
    return;
}

int main(){
    int status;
    pid_t child_id;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    char cwd[PATH_MAX];

    if ((chdir(getcwd(cwd, PATH_MAX))) < 0) {
        exit(EXIT_FAILURE);
    }

    // A
    if (child_id == 0){
        create_dir();
    }else{
        while((wait(&status)) > 0);
        int status_unzip;
        pid_t ch_unzip = fork();

        // B
        if(ch_unzip == 0){
            unzip();
        }else{
            while((wait(&status_unzip)) > 0);
            int status_move;
            pid_t ch_move = fork();
            //move_animals();
            // C
            //printf("%d\n", ch_move);
            if(ch_move == 0){
               move_animals(); 
            }else{
                while((wait(&status_move)) > 0);
                int status_del_bird;
                pid_t ch_delete_bird = fork();

                // D
                if(ch_delete_bird == 0){
                    delete_bird();
                }else{
                    while((wait(&status_del_bird)) > 0);
                    // E
                    create_list();
                }
            }
        }
    }
}
